<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
        'id',
        'name',
        'model',
        'provider',
        'brand',
        'brand_id',
        'location',
        'cpu',
        'drive',
        'ram',
        'price',
    ];

    public function brandname()
    {
        return $this->belongsTo('App\Brand', 'brand_id');
    }
}
