<?php

namespace App\Http\Controllers;

use App\Product;
use App\Brand;
use Illuminate\Http\Request;


class ProductController extends Controller
{

    public function index()
    {
        $products = Product::paginate(8);

        return view('products.products', ['products' => $products]);
    }


    public function show(Product $product)
    {
        return view('products.product_show', compact('product'));
    }


    public function create()
    {
        $brands = Brand::all();

        return view('products.product_create', ['brands' => $brands ]);
    }


    public function store(Request $request)
    {
        if ($request->isMethod('post')) {
            $input = $request->except('_token');
            $product = new Product();
            $product->fill($input);

            /* creating a new product key */
            $key = md5(str_replace(" ", "", $input['provider'].$input['location'].$input['brand'].$input['model'].$input['cpu'].$input['drive'].$input['ram']));

            if ($this->checkNewKey($key)){
                $product->key = $key;
            } else {

                return redirect('/admin/create/')->withErrors('The same Product key is set');
            }

            if ($product->save()) {

                return redirect('/admin')->with('status', 'Product was create');
            } else {

                return redirect('/admin')->withErrors('Product wasn\'t create');
            }
        }
    }


    public function edit(Product $product)
    {
        $brands = Brand::all();

        return view('products.product_edit', compact('product'), ['brands' => $brands ]);
    }


    public function update(Request $request, Product $product)
    {
        if ($request->isMethod('POST')) {

            $input = $request->except('_token');
            $product->fill($input);

            /* creating a new product key */
            $key = md5(str_replace(" ", "", $input['provider'].$input['location'].$input['brand'].$input['model'].$input['cpu'].$input['drive'].$input['ram']));

            if ($this->checkNewKey($key, $product->id)){

                $product->key = $key;
            } else {

                return redirect('/admin/edit/'.$product->id)->withErrors('The same Product key is set');
            }

            if ($product->save()) {

                return redirect('/admin')->with('status', 'Product was update');
            }
        }

        return redirect('/admin')->withErrors('Product wasn\'t update');
    }


    public function destroy(Request $request, Product $product)
    {
        if ($request->isMethod('DELETE')) {

            $product->delete();

            return redirect('/admin/')->with('status', 'The Product was deleted');
        }

        return redirect('/admin/')->withErrors('Product wasn\'t delete');
    }

    /**
     * checks if is set product has the same key
     */
    public function checkNewKey($newkey, $id=null)
    {
        $keys = $id ? Product::all()->except($id)->pluck('key')->toArray() : Product::all()->pluck('key')->toArray();

        return in_array($newkey, $keys) ? false : true;
    }
}
