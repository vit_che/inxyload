<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Support\Facades\DB;

class MainController extends Controller
{
    public function index()
    {
        $products = Product::paginate(15);

        return view('main', ['products' => $products]);
    }


    public function trunkate()
    {
        DB::statement('ALTER TABLE `products` AUTO_INCREMENT = 1');
        DB::statement('ALTER TABLE `brands` AUTO_INCREMENT = 1');
        DB::delete('delete from products');
        DB::delete('delete from brands');

        return redirect('/')->with('status', 'The DB was trunkated');
    }
}
