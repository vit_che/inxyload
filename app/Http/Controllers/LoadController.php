<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Support\Facades\DB;

class LoadController extends Controller
{
    public function loadData()
    {
        $json = file_get_contents(env('JSON_SOURCE'));

        if($json){

            $data_array = json_decode($json, true)['data'];
            $product_keys = Product::all()->pluck('key')->toArray();   // old products keys
            $products = Product::all()->toArray();  // old products array

            $products_new = [];                     // new products array
            $product_keys_new = [];                 // new product keys array

            $brand_names = [];                      // brand names array
            $brands_array = [];                     // brands array

            $j = 1;

            foreach ($data_array as $item) {

                /* create data for brands table */
                if (!in_array($item['brand'], $brand_names)) {
                    $brand_names[] = $item['brand'];
                    $brands_array[] = [
                        'id' => $j,
                        'name' => $item['brand']
                    ];
                    $j++;
                }

                /* create unique key for Product */
                $item['key'] = md5(str_replace(" ", "", $item['provider'].$item['location'].$item['brand'].$item['model'].$item['cpu'].$item['drive'].$item['ram']));

                if (!in_array($item['key'], $product_keys_new)) {

                    $product_keys_new[] = $item['key'];

                    $products_new[] = [
                        'key' => $item['key'],
                        'provider' => $item['provider'],
                        'location' => $item['location'],
                        'brand' => $item['brand'],
                        'brand_id' => $this->getArrayItem($item['brand'], $brands_array, 'name')['id'],
                        'model' => $item['model'],
                        'cpu' => $item['cpu'],
                        'drive' => $item['drive'],
                        'ram' => $item['ram'],
                        'price' => $item['price']
                    ];
                }
            }

            $updated = 0;
            $saved = 0;
            $deleted = 0;
            $addednew = 0;

            foreach($product_keys as $old){
                if(in_array($old, $product_keys_new)){

                    $old_price = $this->getArrayItem($old, $products, 'key')['price'];
                    $new_price = $this->getArrayItem($old, $products_new, 'key')['price'];
                    if ($old_price != $new_price){
                        $updated++;
                    } else {
                        $saved++;
                    }

                } else {
                    $deleted++;
                }
            }

            foreach ($product_keys_new as $new){
                if(!in_array($new, $product_keys)){
                    $addednew++;
                }
            }

            /* trunkate the products table */
            DB::statement('ALTER TABLE `products` AUTO_INCREMENT = 1');
            DB::delete('delete from products');
            /* SAVE PRODUCTS */
            DB::table('products')->insert($products_new);

            /* trunkate the brands table */
            DB::statement('ALTER TABLE `brands` AUTO_INCREMENT = 1');
            DB::delete('delete from brands');
            /* SAVE BRANDS */
            DB::table('brands')->insert($brands_array);

            return redirect('/')->with('status', 'DB was updated. '.$addednew.' were added new products. '.$updated.' were updated and '.$deleted.' were deleted products.');
        }
    }


    public function getArrayItem($needle, $array, $column)
    {
        $key = array_search($needle, array_column($array, $column));

        return $array[$key];
    }
}
