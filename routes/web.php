<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});


Route::get('/', 'MainController@index')->name('main');

Route::get('/load', 'LoadController@loadData')->name('load');

Route::get('/trunkate', 'MainController@trunkate')->name('trunkate');

Route::group(['prefix' => 'admin'], function () {

    Route::get('/', 'ProductController@index')->name('products');

    Route::get('/show/{product}', 'ProductController@show')->name('product_show');

    Route::get('/create', 'ProductController@create')->name('product_create');

    Route::post('/store', 'ProductController@store')->name('product_store');

    Route::get('/edit/{product}', 'ProductController@edit')->name('product_edit');

    Route::post('/update/{product}', 'ProductController@update')->name('product_update');

    Route::delete('/destroy/{product}', 'ProductController@destroy')->name('product_destroy');
});
