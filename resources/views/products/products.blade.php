@extends('layouts.basic')

@section('content')

    <br>
    <div class="text-center">
        <h4>ADMIN</h4>
        <span id="span" >products page</span>
    </div>
    <br>
    <div class="text-center">
        <a class="btn btn-primary" href="{{ route('product_create') }}">Create new Product</a>
        <a class="btn btn-secondary" href="{{ route('main') }}">Back to Main Page</a>
    </div>
    <br>
    @if(count($products) > 0)

        <div class="container">
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">Provider</th>
                    <th scope="col">Brand</th>
                    <th scope="col">Brand_ALT</th>
                    <th scope="col">Location</th>
                    <th scope="col">CPU</th>
                    <th scope="col">Drive</th>
                    <th scope="col">Price</th>
                </tr>
                </thead>
                @foreach($products as $product)

                    <tr id="prod_{{ $product->id }}" class="act"  onclick="show({{$product->id}})" title="row is clickable">
                        <td>{{ $product->provider }}</td>
                        <td>{{ $product->brand }}</td>
                        <td>{{ $product->brandname->name }}</td>
                        <td>{{ $product->location }}</td>
                        <td>{{ $product->cpu }}</td>
                        <td>{{ $product->drive }}</td>
                        <td>{{ $product->price }}</td>
                    </tr>

                    <tr id="rud_{{ $product->id }}" class="rud" >
                        <td><a class="btn btn-primary" href="{{ route('product_show', $product) }}">Show</a></td>
                        <td></td>
                        <td></td>
                        <td><a class="btn btn-warning" href="{{ route('product_edit', $product) }}">Edit</a></td>
                        <td>
                            <form action="{{ route('product_destroy', $product) }}" method="POST">
                                @csrf
                                @method('DELETE')
                                <button class="btn btn-danger" onclick="return confirm('Are you sure?')" title="Delete">Delete</button>
                            </form>
                        </td>
                        <td></td>
                        <td></td>
                    </tr>
                @endforeach
            </table>
            <div class="container text-center">{{ $products->links() }}</div>
        </div>
    @else
        <div class="text-center"><span>There aren't products in the DB</span></div>
    @endif

@endsection
