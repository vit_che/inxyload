@extends('layouts.basic')

@section('content')

    <br>
    <div class="flex-center position-ref full-height">
        <div class="text-center">
            <a class="btn btn-secondary" href="{{ route('main') }}">Back to Main Page</a>
            <a class="btn btn-secondary" href="{{ route('products') }}">Back to Products List</a>
        </div>
    </div>
    <br>
    <div class="text-center"><h3>Show Product</h3></div>
    <br>
    @if($product)

        <div class="container text-center">
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">NAME</th>
                        <th scope="col">VALUE</th>
                    </tr>
                </thead>
                    <tr>
                        <td>Provider</td>
                        <td>{{ $product->provider }}</td>
                    </tr>
                <tr>
                    <td>Brand</td>
                    <td>{{ $product->brand }}</td>
                </tr>
                <tr>
                    <td>Location</td>
                    <td>{{ $product->location }}</td>
                </tr>
                <tr>
                    <td>Brand</td>
                    <td>{{ $product->brand }}</td>
                </tr>
                <tr>
                    <td>CPU</td>
                    <td>{{ $product->cpu }}</td>
                </tr>
                <tr>
                    <td>Drive</td>
                    <td>{{ $product->drive }}</td>
                </tr>
                <tr>
                    <td>RAM</td>
                    <td>{{ $product->ram }}</td>
                </tr>
                <tr>
                    <td>Price</td>
                    <td>{{ $product->price }}</td>
                </tr>
                <tr>
                    <td>
                        <a class="btn btn-warning" href="{{ route('product_edit', $product) }}">Edit</a>
                    </td>
                    <td>
                        <form action="{{ route('product_destroy', $product) }}" method="POST">
                            @csrf
                            @method('DELETE')
                            <button class="btn btn-danger" onclick="return confirm('Are you sure?')" title="Delete">Delete</button>
                        </form>
                    </td>
                </tr>
            </table>
{{--        </div>--}}
    @else
        <div class="text-center"><span>There aren't product</span></div>
    @endif

@endsection
