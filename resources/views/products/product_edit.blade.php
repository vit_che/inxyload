@extends('layouts.basic')

@section('content')

    <br>
    <div class="flex-center position-ref full-height">
        <div class="text-center">
            <a class="btn btn-secondary" href="{{ route('main') }}">Back to Main Page</a>
            <a class="btn btn-secondary" href="{{ route('products') }}">Back to Products List</a>
        </div>
    </div>
    <br>
    <div class="text-center"><h3>Edit Product Form</h3></div>
    <br>

    <div class="container">
        <form action="{{ route('product_update' , $product) }}"  method="post" >
            @csrf
            {{--  MODEL--}}
            <div class="form-group">
                <div class="row">
                    <div class="col-sm-1"></div>
                    <div class="col-sm-2">
                        <label for="model">MODEL</label>
                    </div>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" id="model" name="model" value="{{ $product->model }}" required>
                    </div>
                </div>
            </div>
            {{-- PROVIDER--}}
            <div class="form-group">
                <div class="row">
                    <div class="col-sm-1"></div>
                    <div class="col-sm-2">
                        <label for="provider">PROVIDER</label>
                    </div>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" id="provider" name="provider" value="{{ $product->provider }}" required>
                    </div>
                </div>
            </div>
            {{-- BRAND--}}
            <div class="form-group">
                <div class="row">
                    <div class="col-sm-1"></div>
                    <div class="col-sm-2">
                        <label for="brand">BRAND</label>
                    </div>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" id="brand" name="brand" value="{{ $product->brand }}" required>
                    </div>
                </div>
            </div>
            {{-- BRAND_ALT --}}
            <div class="form-group">
                <div class="row">
                    <div class="col-sm-1"></div>
                    <div class="col-sm-2">
                        <label for="brand">BRAND_ALT</label>
                    </div>
                    <div class="col-sm-8">
                        @if($brands)
                            <select class="form-control" name="brand_id">
                                @foreach( $brands as $brand)
                                    <option value="{{ $brand->id }}"> {{ $brand->name }} </option>
                                @endforeach
                            </select>
                        @endif
                    </div>
                </div>
            </div>
            {{-- LOCATION--}}
            <div class="form-group">
                <div class="row">
                    <div class="col-sm-1"></div>
                    <div class="col-sm-2">
                        <label for="location">LOCATION</label>
                    </div>
                    <div class="col-sm-8">
                        <input type="text" class="typeahead form-control" id="location" name="location" value="{{ $product->location }}">
                        <span id="local_error" class="alert-danger"></span>
                    </div>
                </div>
            </div>
            {{-- CPU --}}
            <div class="form-group">
                <div class="row">
                    <div class="col-sm-1"></div>
                    <div class="col-sm-2">
                        <label for="cpu">CPU</label>
                    </div>
                    <div class="col-sm-8">
                        <input type="text" class="typeahead form-control" id="cpu" name="cpu" value="{{ $product->cpu }}">
                        <span id="local_error" class="alert-danger"></span>
                    </div>
                </div>
            </div>
            {{-- DRIVE --}}
            <div class="form-group">
                <div class="row">
                    <div class="col-sm-1"></div>
                    <div class="col-sm-2">
                        <label for="drive">DRIVE</label>
                    </div>
                    <div class="col-sm-8">
                        <input type="text" class="typeahead form-control" id="drive" name="drive" value="{{ $product->drive }}">
                        <span id="local_error" class="alert-danger"></span>
                    </div>
                </div>
            </div>
            {{-- RAM --}}
            <div class="form-group">
                <div class="row">
                    <div class="col-sm-1"></div>
                    <div class="col-sm-2">
                        <label for="ram">RAM</label>
                    </div>
                    <div class="col-sm-8">
                        <input type="number" class="form-control" id="ram" name="ram" value="{{ $product->ram }}" required>
                    </div>
                </div>
            </div>
            {{-- PRICE --}}
            <div class="form-group">
                <div class="row">
                    <div class="col-sm-1"></div>
                    <div class="col-sm-2">
                        <label for="price">PRICE</label>
                    </div>
                    <div class="col-sm-8">
                        <input type="text" class="typeahead form-control" id="price" name="price" value="{{ $product->price }}">
                        <span id="local_error" class="alert-danger"></span>
                    </div>
                </div>
            </div>

            {{--  SAVE BUTTON--}}
            <div class="form-group">
                <div class="row">
                    <div class="col-sm-3"></div>
                    <div class="col-sm-offset-0 col-sm-8">
                        <input type="submit" class="btn btn-primary" value="Save">
                    </div>
                </div>
            </div>
        </form>
    </div>

@endsection
