@extends('layouts.basic')

@section('content')
    <br>
    <div class="text-center"><span>MAIN PAGE</span></div>
    <br>
    <div class="text-center">
        <a class="btn btn-primary" href="{{ route('load') }}">Load data to DB</a>
        <a class="btn btn-danger" href="{{ route('trunkate') }}">Clean DB</a>
        <a class="btn btn-secondary" href="{{ route('products') }}">Admin Products List</a>
    </div>
    <br>
    @if(count($products) > 0)

        <div class="container">
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">Provider</th>
                    <th scope="col">Brand</th>
                    <th scope="col">Location</th>
                    <th scope="col">CPU</th>
                    <th scope="col">Drive</th>
                    <th scope="col">Price</th>
                </tr>
                </thead>
                @foreach($products as $product)
                    <tr>
                        <td>{{ $product->provider }}</td>
                        <td>{{ $product->brand }}</td>
                        <td>{{ $product->location }}</td>
                        <td>{{ $product->cpu }}</td>
                        <td>{{ $product->drive }}</td>
                        <td>{{ $product->price }}</td>
                    </tr>
                @endforeach
            </table>
            <div class="container text-center">{{ $products->links() }}</div>
        </div>
    @else
        <div class="text-center"><span>There aren't products in the DB</span></div>
    @endif

@endsection()
